var exec = require('cordova/exec');
module.exports = {
	loadUrl: function (ip, user, password, cameraName, success, error) {
		exec(success, error, "VLCPlugin", "loadUrl", [ip, user, password, cameraName]);
	}
};