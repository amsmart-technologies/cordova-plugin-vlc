package cordova.plugin.vlc;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class VideoFinder {

    private DahuaService dahuaService;
    private String finderToken;

    public interface Callback {
        void message(String message);
    }

    private AsyncHttpClient client;
    private List<JSONObject> videos;

    public VideoFinder(DahuaService dahuaService) {
        this.client = new AsyncHttpClient();
        this.videos = new ArrayList<>();
        this.dahuaService = dahuaService;
    }

    private void clearVideos() {
        this.videos.clear();
    }

    public void getVideos(String startDate, String endDate, Callback callback, int attempts) {
        clearVideos();
        createFinder(new DahuaService.DahuaResponseHandler() {
            @Override
            public void getResult(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200) {
                    finderToken = new String(responseBody).replaceAll("\r", "").replaceAll("\n", "").split("=")[1];
                    startFind(startDate, endDate, finderToken, new DahuaService.DahuaResponseHandler() {
                        @Override
                        public void getResult(int statusCode, Header[] headers, byte[] responseBody) {
                            if (statusCode == 200) {
                                findNext(finderToken, 100, new DahuaService.DahuaResponseHandler() {
                                    @Override
                                    public void getResult(int statusCode, Header[] headers, byte[] responseBody) {
                                        if (statusCode == 200) {
                                            String[] results = new String(responseBody).replaceAll("\r", "").split("\n");

                                            if (new String(responseBody).contains("Invalid session")) {
                                                destroyFinder(finderToken, new DahuaService.DahuaResponseHandler() {
                                                    @Override
                                                    public void getResult(int statusCode, Header[] headers, byte[] responseBody) {
                                                        getVideos(startDate, endDate, callback, attempts - 1);
                                                    }
                                                });
                                                return;
                                            }

                                            int videoCount = Integer.parseInt(results[0].split("=")[1]);
                                            results = Arrays.copyOfRange(results, 1, results.length);

                                            int index = 0;
                                            JSONObject video = new JSONObject();
                                            for (String result: results) {
                                                String[] item = result.replaceFirst("\\.", "#").split("#");
                                                int videoIndex = Integer.parseInt(item[0].replace("items[", "").replace("]", ""));
                                                if (videoIndex > index) {
                                                    videos.add(video);
                                                    video = new JSONObject();
                                                    index += 1;
                                                }
                                                String[] keyValue = item[1].split("=");
                                                try {
                                                    video.put(keyValue[0], keyValue[1]);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }

                                            // Add last video if exists
                                            if (videoCount > 0) {
                                                videos.add(video);
                                            }

                                            destroyFinder(finderToken, new DahuaService.DahuaResponseHandler() {
                                                @Override
                                                public void getResult(int statusCode, Header[] headers, byte[] responseBody) {
                                                    callback.message(String.valueOf(videos.size()));
                                                }
                                            });
                                        } else {
                                            destroyFinder(finderToken, new DahuaService.DahuaResponseHandler() {
                                                @Override
                                                public void getResult(int statusCode, Header[] headers, byte[] responseBody) {
                                                    if (attempts > 0) {
                                                        getVideos(startDate, endDate, callback, attempts - 1);
                                                    } else {
                                                        callback.message(String.valueOf(videos.size()));
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            } else {
                                destroyFinder(finderToken, new DahuaService.DahuaResponseHandler() {
                                    @Override
                                    public void getResult(int statusCode, Header[] headers, byte[] responseBody) {
                                        if (attempts > 0) {
                                            getVideos(startDate, endDate, callback, attempts - 1);
                                        } else {
                                            callback.message(String.valueOf(videos.size()));
                                        }
                                    }
                                });
                            }
                        }
                    });
                } else {
                    destroyFinder(finderToken, new DahuaService.DahuaResponseHandler() {
                        @Override
                        public void getResult(int statusCode, Header[] headers, byte[] responseBody) {
                            if (attempts > 0) {
                                getVideos(startDate, endDate, callback, attempts - 1);
                            } else {
                                callback.message(String.valueOf(videos.size()));
                            }
                        }
                    });
                }
            }
        });
    }

    private void createFinder(DahuaService.DahuaResponseHandler responseHandler) {
        String url = dahuaService.getBaseUrl() + DahuaService.DahuaUrl.VIDEO_CREATE_FINDER;
        client.setBasicAuth(dahuaService.getUsername(), dahuaService.getPassword());
        client.get(url, responseHandler);
    }

    private void startFind(String startDate, String endDate, String token, DahuaService.DahuaResponseHandler responseHandler) {
        String url = dahuaService.getBaseUrl() + DahuaService.DahuaUrl.VIDEO_START_FINDER
                .replace("[FINDER_ID]", token)
                .replace("[DATE_START]", startDate.replace(" ", "%20"))
                .replace("[DATE_END]", endDate.replace(" ", "%20"));
        client.setBasicAuth(dahuaService.getUsername(), dahuaService.getPassword());
        client.get(url, responseHandler);
    }

    private void findNext(String token, int count, DahuaService.DahuaResponseHandler responseHandler) {
        String url = dahuaService.getBaseUrl() + DahuaService.DahuaUrl.VIDEO_FIND_NEXT
                .replace("[FINDER_ID]", token)
                .replace("[COUNT]", String.valueOf(count));
        client.setBasicAuth(dahuaService.getUsername(), dahuaService.getPassword());
        client.get(url, responseHandler);
    }

    private void destroyFinder(String token, DahuaService.DahuaResponseHandler responseHandler) {
        String url = dahuaService.getBaseUrl() + DahuaService.DahuaUrl.VIDEO_DESTROY_FINDER.replace("[FINDER_ID]", token);
        client.setBasicAuth(dahuaService.getUsername(), dahuaService.getPassword());
        client.get(url, responseHandler);
    }

    public List<JSONObject> getVideos() {
        return videos;
    }
}
