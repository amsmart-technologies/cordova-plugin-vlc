package cordova.plugin.vlc;

import android.widget.SeekBar;

import com.loopj.android.http.AsyncHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

public class VideoColorSeekBarListener implements SeekBar.OnSeekBarChangeListener {

    private String config;
    private DahuaService dahuaService;

    public VideoColorSeekBarListener(String config, DahuaService dahuaService) {
        this.config = config;
        this.dahuaService = dahuaService;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        seekBar.setAlpha(0.5f);
        dahuaService.setVideoColorConfig(config, seekBar.getProgress(), new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                seekBar.setAlpha(1f);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                onStopTrackingTouch(seekBar);
            }
        });
    }
}