package cordova.plugin.vlc;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;

public class VLCPlugin extends CordovaPlugin {
    private String TAG = " [ VLCPlugin ] ";
    private static final int RESULT_OK = 0;
    private CallbackContext ctx;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        if (action.equals("loadUrl")) {
            Log.d(TAG, "loadUrl");
            try {
                String ip = args.getString(0);
                String user = args.getString(1);
                String password = args.getString(2);
                String cameraName = args.getString(3);
                Context context = cordova.getActivity();
                Intent intent = new Intent(context, VLCActivity.class);
                intent.putExtra("ip", ip);
                intent.putExtra("user", user);
                intent.putExtra("password", password);
                intent.putExtra("cameraName", cameraName);

                ctx = callbackContext;
                cordova.getActivity().startActivityForResult(intent, 1);
            } catch (Exception e) {
                callbackContext.error(e.toString());
            }
        } else {
            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                ctx.success("ok");
            }
        }
    }
}
