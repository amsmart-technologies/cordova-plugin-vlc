package cordova.plugin.vlc;

import android.Manifest;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;

import android.graphics.Color;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;

import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;

import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentContainerView;

import org.json.JSONException;
import org.json.JSONObject;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;
import org.videolan.libvlc.interfaces.IVLCVout;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import cordova.plugin.vlc.fragments.VideoLiveFragment;
import cordova.plugin.vlc.fragments.VideoRecordsFragment;

public class VLCActivity extends AppCompatActivity implements IVLCVout.Callback, VideoLiveFragment.VideoLiveFragmentActionListener, VideoRecordsFragment.VideoRecordsFragmentActionListener {

    public static int STORAGE_PERMISSION_CODE = 101;

    private LibVLC libVLC;
    private MediaPlayer mediaPlayer;
    private String ip;
    private String user;
    private String password;
    private String cameraName;
    private String currentFragment;
    private boolean videoPlaying;
    private boolean videoMainStream;
    private boolean storagePermissionEnabled;
    private boolean isLandscape;
    private Intent resultIntent = new Intent();
    private File fileSaved;
    private SurfaceView surfaceView;
    private ProgressBar progressBarVideo;
    private ImageView imageCreated;
    private LinearLayout layoutCameraFade;
    private LinearLayout layoutCreated;
    private Button buttonFragmentLive;
    private Button buttonFragmentRecords;
    private VideoLiveFragment videoLiveFragment;
    private VideoRecordsFragment videoRecordsFragment;
    private FragmentContainerView fragmentContainer;
    private Bundle userBundle;

    private JSONObject savedVideoForLandscape;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getResources("vlc_activity", "layout"));
        getSupportActionBar().hide();
        getWindow().setStatusBarColor(0xff00274e);

        Intent receivedIntent = getIntent();
        ip = receivedIntent.getStringExtra("ip");
        user = receivedIntent.getStringExtra("user");
        password = receivedIntent.getStringExtra("password");
        cameraName = receivedIntent.getStringExtra("cameraName");
        userBundle = new Bundle();
        userBundle.putString("ip", ip);
        userBundle.putString("user", user);
        userBundle.putString("password", password);

        videoPlaying = true;
        videoMainStream = true;
        isLandscape = false;

        surfaceView = (SurfaceView) findViewById(getResources("surfaceView", "id"));
        progressBarVideo = (ProgressBar) findViewById(getResources("progressBarVideo", "id"));
        layoutCameraFade = (LinearLayout) findViewById(getResources("layoutCameraFade", "id"));
        layoutCreated = (LinearLayout) findViewById(getResources("layoutCreated", "id"));
        layoutCreated.animate().translationX(3000).setDuration(0).start();
        imageCreated = (ImageView) findViewById(getResources("imageCreated", "id"));

        videoLiveFragment = new VideoLiveFragment(this, userBundle, getResources("fragment_video_live", "layout"));
        videoRecordsFragment = new VideoRecordsFragment(this, userBundle, getResources("fragment_video_records", "layout"));
        fragmentContainer = (FragmentContainerView) findViewById(getResources("fragmentContainer", "id"));
        buttonFragmentLive = (Button) findViewById(getResources("buttonFragmentLive", "id"));
        buttonFragmentRecords = (Button) findViewById(getResources("buttonFragmentRecords", "id"));

        if (savedInstanceState != null) {
            videoPlaying = savedInstanceState.getBoolean("videoPlaying");
            videoMainStream = savedInstanceState.getBoolean("videoMainStream");
            storagePermissionEnabled = savedInstanceState.getBoolean("storagePermissionEnabled");
            isLandscape = savedInstanceState.getBoolean("isLandscape");
            currentFragment = savedInstanceState.getString("currentFragment");

            try {
                if (savedInstanceState.containsKey("savedVideoForLandscape")) {
                    savedVideoForLandscape = new JSONObject(savedInstanceState.getString("savedVideoForLandscape"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (currentFragment.equals("records")) {
                currentFragment = "NO_FRAGMENT";
                toFragmentRecords(null);
            } else {
                currentFragment = "NO_FRAGMENT";
                toFragmentLive(null);
            }
        } else {
            currentFragment = "NO_FRAGMENT";
            toFragmentLive(null);
        }

        libVLC = new LibVLC(this, getVLCConfig());
        mediaPlayer = new MediaPlayer(libVLC);
    }

    public ArrayList<String> getVLCConfig() {
        ArrayList<String> config = new ArrayList<>();
        config.add("--no-drop-late-frames");
        config.add("--no-skip-frames");
        config.add("--rtsp-tcp");
        config.add("-vvv");
        return config;
    }

    public void toFragmentLive(View view) {
        if (currentFragment.equals("live")) return;
        currentFragment = "live";
        buttonFragmentLive.setBackground(ContextCompat.getDrawable(getApplicationContext(), getResources("segment_active", "drawable")));
        buttonFragmentLive.setTextColor(Color.parseColor("#00274e"));
        buttonFragmentRecords.setBackground(ContextCompat.getDrawable(getApplicationContext(), getResources("segment_inactive", "drawable")));
        buttonFragmentRecords.setTextColor(Color.parseColor("#FFFFFF"));
        savedVideoForLandscape = null;
        getSupportFragmentManager().beginTransaction().setCustomAnimations(
                getResources("slide_left", "anim"),  // enter
                getResources("slide_right", "anim")// exit
        ).replace(getResources("fragmentContainer", "id"), videoLiveFragment).commit();
        if (mediaPlayer != null && view != null) {
            progressBarVideo.setVisibility(View.VISIBLE);
            layoutCameraFade.setVisibility(View.VISIBLE);
            setLiveRTSPMedia();
            enableBlackScreen(false);
        }
    }

    public void toFragmentRecords(View view) {
        if (currentFragment.equals("records")) return;
        currentFragment = "records";
        buttonFragmentLive.setBackground(ContextCompat.getDrawable(getApplicationContext(), getResources("segment_inactive", "drawable")));
        buttonFragmentLive.setTextColor(Color.parseColor("#FFFFFF"));
        buttonFragmentRecords.setBackground(ContextCompat.getDrawable(getApplicationContext(), getResources("segment_active", "drawable")));
        buttonFragmentRecords.setTextColor(Color.parseColor("#00274e"));
        progressBarVideo.setVisibility(View.INVISIBLE);
        layoutCameraFade.setVisibility(View.INVISIBLE);
        getSupportFragmentManager().beginTransaction().setCustomAnimations(
                getResources("slide_left", "anim"),  // enter
                getResources("slide_right", "anim")  // exit
        ).replace(getResources("fragmentContainer", "id"), videoRecordsFragment).commit();
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            if (savedVideoForLandscape != null) {
                setPlaybackRTSPMedia(savedVideoForLandscape);
            }
            enableBlackScreen(true);
        }
    }

    public void checkStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE }, STORAGE_PERMISSION_CODE);
            }
        } else {
            storagePermissionEnabled = true;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == STORAGE_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                storagePermissionEnabled = true;
            } else {
                storagePermissionEnabled = false;
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        savedInstanceState.putBoolean("videoPlaying", videoPlaying);
        savedInstanceState.putBoolean("videoMainStream", videoMainStream);
        savedInstanceState.putBoolean("storagePermissionEnabled", storagePermissionEnabled);
        savedInstanceState.putBoolean("isLandscape", isLandscape);
        savedInstanceState.putString("currentFragment", currentFragment);
        if (savedVideoForLandscape != null) {
            savedInstanceState.putString("savedVideoForLandscape", savedVideoForLandscape.toString());
        }
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setStreamSurfaceSize() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        if (isLandscape) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            IVLCVout vout = mediaPlayer.getVLCVout();
            vout.setWindowSize(width,height);
        } else {
            IVLCVout vout = mediaPlayer.getVLCVout();
            vout.setWindowSize(width,(width * 1080) / 1920);
            surfaceView.getLayoutParams().height = (width * 1080) / 1920;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onStart() {
        super.onStart();
        checkStoragePermission();
        IVLCVout vout = mediaPlayer.getVLCVout();
        vout.setVideoView(surfaceView);
        vout.attachViews();
        vout.addCallback(this);
        setStreamSurfaceSize();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mediaPlayer.stop();
        mediaPlayer.detachViews();
        setResult(0, resultIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mediaPlayer.release();
        libVLC.release();
        setResult(0, resultIntent);
    }

    private int getResources(String name, String type) {
        return getApplication().getResources().getIdentifier(name, type, getApplication().getPackageName());
    }

    @Override
    public void onSurfacesCreated(IVLCVout vlcVout) {
        if (currentFragment.equals("live")) {
            setLiveRTSPMedia();
        } else {
            if (savedVideoForLandscape != null) {
                setPlaybackRTSPMedia(savedVideoForLandscape);
            }
        }
    }

    public String getBaseRTSPUrl (){
        return "rtsp://" + user + ":" + password + "@" + ip;
    }

    public void setLiveRTSPMedia() {
        String url = getBaseRTSPUrl() + (videoMainStream ? DahuaService.DahuaUrl.RTSP_STREAM : DahuaService.DahuaUrl.RTSP_STREAM_SUB_STREAM);
        Uri uri = Uri.parse(url);
        Media media = new Media(libVLC, uri);
        media.setHWDecoderEnabled(true, true);
        media.addOption(":network-caching=150");
        media.addOption(":clock-jitter=0");
        media.addOption(":clock-synchro=0");
        mediaPlayer.setMedia(media);
        mediaPlayer.play();
        mediaPlayer.setEventListener(new MediaPlayer.EventListener() {
            @Override
            public void onEvent(MediaPlayer.Event event) {
                if (event.type == MediaPlayer.Event.Vout) {
                    enableLoadingLayout(false);
                } else if (event.type == MediaPlayer.Event.Buffering) {
                    enableLoadingLayout(true);
                } else if (event.type == MediaPlayer.Event.TimeChanged || event.type == MediaPlayer.Event.PositionChanged) {
                    enableLoadingLayout(false);
                }
            }
        });
    }

    public void setPlaybackRTSPMedia(JSONObject video) {
        savedVideoForLandscape = video;

        String urlPlayback = null;
        try {
            String start = video.getString("StartTime")
                    .replaceAll("-", "_")
                    .replaceAll(" ", "_")
                    .replaceAll(":", "_");

            String end = video.getString("EndTime")
                    .replaceAll("-", "_")
                    .replaceAll(" ", "_")
                    .replaceAll(":", "_");

            urlPlayback = DahuaService.DahuaUrl.RTSP_PLAYBACK.replace("[DATE_START]", start).replace("[DATE_END]", end);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String url = getBaseRTSPUrl() + urlPlayback;
        Uri uri = Uri.parse(url);
        Media media = new Media(libVLC, uri);
        media.setHWDecoderEnabled(true, true);
        media.addOption(":network-caching=150");
        media.addOption(":clock-jitter=150");
        media.addOption(":clock-synchro=0");
        mediaPlayer.setMedia(media);
        mediaPlayer.play();
        mediaPlayer.setEventListener(new MediaPlayer.EventListener() {
            @Override
            public void onEvent(MediaPlayer.Event event) {
                if (event.type == MediaPlayer.Event.Vout) {
                    enableLoadingLayout(false);
                } else if (event.type == MediaPlayer.Event.PositionChanged || event.type == MediaPlayer.Event.TimeChanged) {
                    videoRecordsFragment.setVLCProgress((int) Math.round(mediaPlayer.getPosition() * 100));
                    enableLoadingLayout(false);
                } else if (event.type == MediaPlayer.Event.Buffering) {
                    enableLoadingLayout(true);
                } else if (event.type == MediaPlayer.Event.Playing) {
                    videoRecordsFragment.setVLCProgress((int) Math.round(mediaPlayer.getPosition() * 100));
                    enableLoadingLayout(false);
                } else if (event.type == MediaPlayer.Event.EndReached) {
                    enableBlackScreen(true);
                    videoRecordsFragment.setEndReached(true);
                    videoRecordsFragment.resetFragment();
                }
            }
        });
        enableBlackScreen(false);
    }

    private void enableLoadingLayout(boolean enable) {
        if (enable) {
            progressBarVideo.setVisibility(View.VISIBLE);
            layoutCameraFade.setVisibility(View.VISIBLE);
        } else {
            progressBarVideo.setVisibility(View.INVISIBLE);
            layoutCameraFade.setVisibility(View.INVISIBLE);
        }
    }

    public void enableBlackScreen(boolean enable) {
        if (enable) {
            surfaceView.setBackgroundColor(Color.parseColor("#000000"));
        } else {
            surfaceView.setBackgroundColor(Color.parseColor("#00000000"));
        }
    }

    @Override
    public void onSurfacesDestroyed(IVLCVout vlcVout) {

    }

    public void openSavedImage(View view) {
        if (storagePermissionEnabled) {
            Intent intent = new Intent(Intent.ACTION_VIEW)//
                .setDataAndType(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ?
                FileProvider.getUriForFile(getApplicationContext(),getPackageName() + ".provider", fileSaved) : Uri.fromFile(fileSaved),
            "image/*").addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(intent);
        }
    }

    public void fullScreen(View view) {
        isLandscape = !isLandscape;
        if (isLandscape) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    public int dpToPx(int px) {
        return (int) (px * (getApplicationContext().getResources().getDisplayMetrics().density));
    }


    @Override
    public void action(JSONObject ob) throws JSONException {
        if (ob.get("tag").equals("cameraSnapshot")) {
            if (ob.get("action").equals("takePicture")) {
                byte[] imageData = Base64.decode(ob.getString("data"), Base64.DEFAULT);
                layoutCameraFade.setVisibility(View.VISIBLE);
                layoutCameraFade.setBackgroundColor(0xFFFFFFFF);
                layoutCameraFade.animate().alpha(0).setDuration(1000).start();
                imageCreated.setImageBitmap(BitmapFactory.decodeByteArray(imageData, 0, imageData.length));
                layoutCreated.animate().translationX(0).setDuration(1000).start();

                if (storagePermissionEnabled) {
                    Date date = Calendar.getInstance().getTime();
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd_hh-mm-ss");
                    String strDate = dateFormat.format(date);

                    File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                    path.mkdirs();

                    File file = new File(path,  cameraName.replaceAll(" ", "").toLowerCase() + "_" + strDate+".jpeg");
                    fileSaved = file;

                    try (FileOutputStream stream = new FileOutputStream(file)) {
                        stream.write(imageData);
                    } catch (IOException e) {
                        Log.e("Picture error", e.getMessage());
                    }
                }
            } else if (ob.get("action").equals("hideCheeseOverlay")) {
                layoutCameraFade.setVisibility(View.INVISIBLE);
                layoutCameraFade.setBackgroundColor(0x77000000);
                layoutCameraFade.animate().alpha(1).setDuration(0).start();
            } else if (ob.get("action").equals("hidePreviewImage")) {
                layoutCreated.animate().translationX(3000).setDuration(1000).start();
            }
        } else if (ob.get("tag").equals("videoPlaying")) {
            if (ob.get("action").equals("start")) {
                enableLoadingLayout(true);
                mediaPlayer.play();
            } else if (ob.get("action").equals("stop")) {
                mediaPlayer.stop();
            }
        } else if (ob.get("tag").equals("startStopRecord")) {
            if (ob.get("action").equals("startRecord")) {
                surfaceView.setBackgroundResource(getResources("background_recording_frame", "drawable"));
            } else if (ob.get("action").equals("stopRecord")) {
                surfaceView.setBackgroundResource(0);
            }
        } else if (ob.get("tag").equals("changeVideoSize")) {
            videoMainStream = !videoMainStream;
            setLiveRTSPMedia();
        } else if (ob.get("tag").equals("playback")) {
            if (ob.get("action").equals("play")) {
                enableLoadingLayout(true);
                setPlaybackRTSPMedia(ob.getJSONObject("video"));
            } else if (ob.get("action").equals("start")) {
                mediaPlayer.play();
            } else if (ob.get("action").equals("stop")) {
                mediaPlayer.pause();
            }
        }
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public boolean isVideoPlaying() {
        return videoPlaying;
    }

    public void setVideoPlaying(boolean videoPlaying) {
        this.videoPlaying = videoPlaying;
    }

    public boolean isLandscape() {
        return isLandscape;
    }

    public JSONObject getSavedVideoForLandscape() {
        return savedVideoForLandscape;
    }

    public boolean isVideoMainStream() {
        return videoMainStream;
    }
}