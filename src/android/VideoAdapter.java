package cordova.plugin.vlc;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VideoViewHolder> {

    public interface OnItemClickListener {
        void click(int position);
    }

    public static class VideoViewHolder extends RecyclerView.ViewHolder {
        private TextView textDate;
        private TextView textDuration;
        private LinearLayout itemContainer;

        public VideoViewHolder(@NonNull View itemView, OnItemClickListener onItemClickListener, VideoAdapter videoAdapter) {
            super(itemView);
            textDate = itemView.findViewById(videoAdapter.getResources("textDate", "id"));
            textDuration = itemView.findViewById(videoAdapter.getResources("textDuration", "id"));
            itemContainer = (LinearLayout) itemView.findViewById(videoAdapter.getResources("itemContainer", "id"));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            videoAdapter.setSelected(position);
                            onItemClickListener.click(position);
                        }
                    }
                }
            });
        }
    }

    private List<JSONObject> videos;
    private OnItemClickListener onItemClickListener;
    private int selected = RecyclerView.NO_POSITION;
    private ViewGroup parent;

    public VideoAdapter(List<JSONObject> videos) {
        this.videos = videos;
    }

    public void setVideos(List<JSONObject> videos) {
        this.videos = videos;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    public int getSelected() {
        return selected;
    }

    public List<JSONObject> getVideos() {
        return videos;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public VideoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.parent = parent;
        View view = LayoutInflater.from(parent.getContext()).inflate(getResources("item_video", "layout"), parent, false);
        VideoViewHolder videoViewHolder = new VideoViewHolder(view, onItemClickListener, this);

        return videoViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull VideoViewHolder holder, int position) {
        JSONObject currentVideo = videos.get(position);
        try {
            holder.textDate.setText(currentVideo.getString("StartTime"));
            holder.textDuration.setText("Duration: " + getFormatedTime(currentVideo.getString("Duration")));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (selected == position) {
            holder.itemContainer.setBackgroundResource(getResources("item_video_selected", "drawable"));
        } else {
            holder.itemContainer.setBackgroundResource(getResources("item_video", "drawable"));
        }
    }

    @Override
    public int getItemCount() {
        return videos.size();
    }

    public String getFormatedTime(String seconds) {
        int duration = Integer.parseInt(seconds);
        int h = (duration / 60) / 60;
        int m = (duration / 60) % 60;
        int s = duration % 60;

        String res = "";

        if (h != 0) {
            res += h + ((h < 2) ? " hour " : " hours ");
        }

        if (m != 0) {
            res += m + ((m < 2) ? " minute " : " minutes ");
        }

        if (s != 0) {
            res += s + ((s < 2) ? " second" : " seconds");
        }

        return res;
    }

    public int getResources(String name, String type) {
        return parent.getResources().getIdentifier(name, type, parent.getContext().getPackageName());
    }

}
