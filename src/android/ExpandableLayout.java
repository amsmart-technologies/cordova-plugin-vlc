package cordova.plugin.vlc;

import android.content.Context;
import android.content.res.Resources;

import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class ExpandableLayout extends LinearLayout {

    private LinearLayout layoutContent;
    private LinearLayout layoutHeader;
    private TextView title;
    private ImageView iconArrow;
    private Resources resources;
    private Context context;

    private boolean open;

    public ExpandableLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.resources = context.getResources();
        this.context = context;
        LayoutInflater.from(context).inflate(getResources("expandable_layout", "layout"), this);

        open = false;
        layoutContent = (LinearLayout) findViewById(getResources("content", "id"));
        layoutHeader = (LinearLayout) findViewById(getResources("header", "id"));
        layoutHeader.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toggle();
            }
        });

        title = (TextView) findViewById(getResources("title", "id"));
        iconArrow = (ImageView) findViewById(getResources("iconArrow", "id"));
    }

    public void toggle() {
        open = !open;
        if (open) {
            layoutContent.setVisibility(VISIBLE);
            iconArrow.animate().rotation(90).setDuration(250).start();
        } else {
            layoutContent.setVisibility(GONE);
            iconArrow.animate().rotation(0).setDuration(250).start();
        }
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        if(layoutContent == null){
            super.addView(child, index, params);
        } else {
            layoutContent.addView(child, index, params);
        }
    }

    public void close() {
        layoutContent.setVisibility(GONE);
        iconArrow.animate().rotation(0).setDuration(0).start();
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    private int getResources(String name, String type) {
        return resources.getIdentifier(name, type, context.getPackageName());
    }
}
