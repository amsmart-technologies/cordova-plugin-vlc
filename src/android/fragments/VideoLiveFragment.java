package cordova.plugin.vlc.fragments;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;

import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import cordova.plugin.vlc.DahuaService;
import cordova.plugin.vlc.VLCActivity;
import cordova.plugin.vlc.VideoColorSeekBarListener;
import cz.msebera.android.httpclient.Header;
import cordova.plugin.vlc.ExpandableLayout;

public class VideoLiveFragment extends Fragment {

    public interface VideoLiveFragmentActionListener {
        void action(JSONObject ob) throws JSONException;
    }

    private VLCActivity vlcActivity;
    private VideoLiveFragmentActionListener videoLiveFragmentActionListener;
    private ObjectAnimator recordButtonAnimator;
    private DahuaService dahuaService;
    private String ip;
    private String user;
    private String password;
    private ImageButton buttonPlayPause;
    private ImageButton buttonTakePicture;
    private ImageButton buttonStreamSize;
    private ImageButton buttonVideoMode;
    private ImageButton buttonStartStopRecord;
    private ExpandableLayout layoutVideoConfig;
    private LinearLayout layoutButtons;
    private Button buttonResetConfig;
    private SeekBar seekBarBrightness;
    private SeekBar seekBarContrast;
    private SeekBar seekBarHue;
    private SeekBar seekBarGamma;
    private SeekBar seekBarSaturation;
    private boolean canTakePicture;
    private boolean recordingVideo;
    private int videoMode;

    public VideoLiveFragment(VLCActivity vlcActivity, Bundle bundle, int layout) {
        super(layout);
        this.vlcActivity = vlcActivity;
        this.ip = bundle.getString("ip");
        this.user = bundle.getString("user");
        this.password = bundle.getString("password");
        this.dahuaService = new DahuaService(user, password, ip);
    }

    public VideoLiveFragment() {

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof VideoLiveFragmentActionListener) {
            videoLiveFragmentActionListener = (VideoLiveFragmentActionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement listener!");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        videoLiveFragmentActionListener = null;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        canTakePicture = true;
        recordingVideo = false;

        videoMode = 0;

        buttonPlayPause = (ImageButton) view.findViewById(getResources("buttonPlayPause", "id"));
        buttonPlayPause.setOnClickListener(this::toggleVideoPlaying);
        buttonTakePicture = (ImageButton) view.findViewById(getResources("buttonTakePicture", "id"));
        buttonTakePicture.setOnClickListener(this::cameraSnapshot);
        buttonStreamSize = (ImageButton) view.findViewById(getResources("buttonStreamSize", "id"));
        buttonStreamSize.setOnClickListener(this::changeVideoSize);
        buttonVideoMode = (ImageButton) view.findViewById(getResources("buttonVideoMode", "id"));
        buttonVideoMode.setOnClickListener(this::changeVideoMode);
        buttonStartStopRecord = (ImageButton) view.findViewById(getResources("buttonStartStopRecord", "id"));
        buttonStartStopRecord.setOnClickListener(this::startStopRecord);
        buttonResetConfig = (Button) view.findViewById(getResources("buttonResetConfig", "id"));
        buttonResetConfig.setOnClickListener(this::resetVideoColorConfig);

        layoutVideoConfig = (ExpandableLayout) view.findViewById(getResources("layoutVideoConfig", "id"));
        layoutVideoConfig.setTitle("Video Color Config");
        layoutVideoConfig.close();
        layoutButtons = (LinearLayout) view.findViewById(getResources("layoutButtons", "id"));

        if (vlcActivity.isLandscape()) {
            layoutVideoConfig.setVisibility(View.GONE);
            layoutButtons.setBackgroundColor(0x00000000);

            buttonTakePicture.setBackgroundResource(getResources("button_transparent", "drawable"));
            buttonPlayPause.setBackgroundResource(getResources("button_transparent", "drawable"));
            buttonStartStopRecord.setBackgroundResource(getResources("button_transparent", "drawable"));
            buttonStreamSize.setBackgroundResource(getResources("button_transparent", "drawable"));
            buttonVideoMode.setBackgroundResource(getResources("button_transparent", "drawable"));

            view.findViewById(getResources("textSnapshot", "id")).setVisibility(View.GONE);
            view.findViewById(getResources("textPlayPause", "id")).setVisibility(View.GONE);
            view.findViewById(getResources("textRecord", "id")).setVisibility(View.GONE);
            view.findViewById(getResources("textVideoSize", "id")).setVisibility(View.GONE);
            view.findViewById(getResources("textColor", "id")).setVisibility(View.GONE);
        }

        if (vlcActivity.isVideoMainStream()) {
            buttonStreamSize.setImageResource(getResources("ic_baseline_crop_7_5_24", "drawable"));
        } else {
            buttonStreamSize.setImageResource(getResources("ic_baseline_crop_din_24", "drawable"));
        }

        seekBarBrightness = (SeekBar) view.findViewById(getResources("seekBarBrightness", "id"));
        seekBarBrightness.setOnSeekBarChangeListener(new VideoColorSeekBarListener(DahuaService.ColorConfig.BRIGHTNESS, dahuaService));
        seekBarContrast = (SeekBar) view.findViewById(getResources("seekBarContrast", "id"));
        seekBarContrast.setOnSeekBarChangeListener(new VideoColorSeekBarListener(DahuaService.ColorConfig.CONTRAST, dahuaService));
        seekBarHue = (SeekBar) view.findViewById(getResources("seekBarHue", "id"));
        seekBarHue.setOnSeekBarChangeListener(new VideoColorSeekBarListener(DahuaService.ColorConfig.HUE, dahuaService));
        seekBarGamma = (SeekBar) view.findViewById(getResources("seekBarGamma", "id"));
        seekBarGamma.setOnSeekBarChangeListener(new VideoColorSeekBarListener(DahuaService.ColorConfig.GAMMA, dahuaService));
        seekBarSaturation = (SeekBar) view.findViewById(getResources("seekBarSaturation", "id"));
        seekBarSaturation.setOnSeekBarChangeListener(new VideoColorSeekBarListener(DahuaService.ColorConfig.SATURATION, dahuaService));
        setConfigSeekBars();
        setLayoutOnCreate();
    }

    public void setConfigSeekBars() {
        dahuaService.getVideoColorConfig(new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String[] results = new String(responseBody).replaceAll("\r", "").split("\n");
                for (String config: results) {
                    if (config.startsWith("table.VideoColor[0][0]")) {
                        String attr = config.split("\\.")[2];
                        String[] attrSplit = attr.split("=");
                        String name = attrSplit[0];
                        String value = attrSplit[1];
                        if (name.equals(DahuaService.ColorConfig.BRIGHTNESS)) {
                            seekBarBrightness.setProgress(Integer.parseInt(value));
                        } else if (name.equals(DahuaService.ColorConfig.CONTRAST)) {
                            seekBarContrast.setProgress(Integer.parseInt(value));
                        } else if (name.equals(DahuaService.ColorConfig.HUE)) {
                            seekBarHue.setProgress(Integer.parseInt(value));
                        } else if (name.equals(DahuaService.ColorConfig.GAMMA)) {
                            seekBarGamma.setProgress(Integer.parseInt(value));
                        } else if (name.equals(DahuaService.ColorConfig.SATURATION)) {
                            seekBarSaturation.setProgress(Integer.parseInt(value));
                        }
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if (statusCode == 401 || statusCode == 0) {
                    setConfigSeekBars();
                }
            }
        });
    }

    public void cameraSnapshot(View view) {
        if (!canTakePicture && view != null) return;
        canTakePicture = false;
        buttonTakePicture.animate().alpha(0.5f).setDuration(250).start();
        dahuaService.snapshot(new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                canTakePicture = true;
                buttonTakePicture.animate().alpha(1f).setDuration(250).start();

                try {
                    videoLiveFragmentActionListener.action(new JSONObject()
                            .put("tag", "cameraSnapshot")
                            .put("action", "takePicture")
                            .put("data", Base64.encodeToString(responseBody, Base64.DEFAULT)));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            videoLiveFragmentActionListener.action(new JSONObject()
                                    .put("tag", "cameraSnapshot")
                                    .put("action", "hideCheeseOverlay"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 1000);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            videoLiveFragmentActionListener.action(new JSONObject()
                                    .put("tag", "cameraSnapshot")
                                    .put("action", "hidePreviewImage"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 5000);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if (statusCode == 401) {
                    cameraSnapshot(null);
                }
            }
        });
    }

    public void toggleVideoPlaying(View view) {
        vlcActivity.setVideoPlaying(!vlcActivity.isVideoPlaying());
        if (vlcActivity.isVideoPlaying()) {
            buttonPlayPause.setImageResource(getResources("ic_baseline_pause_24", "drawable"));
        } else {
            buttonPlayPause.setImageResource(getResources("ic_baseline_play_arrow_24", "drawable"));
        }

        try {
            videoLiveFragmentActionListener.action(new JSONObject()
                    .put("tag", "videoPlaying")
                    .put("action", vlcActivity.isVideoPlaying() ? "start" : "stop"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startStopRecord(View view) {
        if (view != null) {
            recordingVideo = !recordingVideo;
        }

        if (recordingVideo) {
            dahuaService.startRecord(new DahuaService.DahuaResponseHandler() {
                @Override
                public void getResult(int statusCode, Header[] headers, byte[] responseBody) {
                    if (statusCode == 401) {
                        startStopRecord(null);
                    }

                    if (statusCode == 200) {
                        buttonStartStopRecord.setBackgroundResource(getResources("button_red_circle", "drawable"));
                        try {
                            videoLiveFragmentActionListener.action(new JSONObject()
                                    .put("tag", "startStopRecord")
                                    .put("action", "startRecord"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        recordButtonAnimator = ObjectAnimator.ofPropertyValuesHolder(
                                buttonStartStopRecord,
                                PropertyValuesHolder.ofFloat("scaleX", 0.75f),
                                PropertyValuesHolder.ofFloat("scaleY", 0.75f));
                        recordButtonAnimator.setDuration(1000);
                        recordButtonAnimator.setRepeatCount(ObjectAnimator.INFINITE);
                        recordButtonAnimator.setRepeatMode(ObjectAnimator.REVERSE);
                        recordButtonAnimator.start();
                    }
                }
            });
        } else {
            dahuaService.stopRecord(new DahuaService.DahuaResponseHandler() {
                @Override
                public void getResult(int statusCode, Header[] headers, byte[] responseBody) {
                    if (statusCode == 401) {
                        startStopRecord(null);
                    }

                    if (statusCode == 200) {
                        try {
                            videoLiveFragmentActionListener.action(new JSONObject()
                                    .put("tag", "startStopRecord")
                                    .put("action", "stopRecord"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        recordButtonAnimator.end();
                        buttonStartStopRecord.setScaleX(1);
                        buttonStartStopRecord.setScaleY(1);
                        buttonStartStopRecord.setBackgroundResource(getResources("button_amsmart", "drawable"));
                    }
                }
            });
        }
    }

    public void changeVideoSize(View view) {
        try {
            if (vlcActivity.isVideoMainStream()) {
                buttonStreamSize.setImageResource(getResources("ic_baseline_crop_din_24", "drawable"));
            } else {
                buttonStreamSize.setImageResource(getResources("ic_baseline_crop_7_5_24", "drawable"));
            }
            videoLiveFragmentActionListener.action(new JSONObject()
                    .put("tag", "changeVideoSize")
                    .put("action", "change"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void changeVideoMode(View view) {
        if (view != null) {
            videoMode++;
            if (videoMode > 2) {
                videoMode = 0;
            }
        }

        dahuaService.setDayNightColor(DahuaService.DayNightColor.values()[videoMode], new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                switch (videoMode) {
                    case 0:
                        buttonVideoMode.setImageResource(getResources("ic_baseline_wb_sunny_24", "drawable"));
                        break;
                    case 1:
                        buttonVideoMode.setImageResource(getResources("ic_baseline_brightness_auto_24", "drawable"));
                        break;
                    case 2:
                        buttonVideoMode.setImageResource(getResources("ic_baseline_nights_stay_24", "drawable"));
                        break;
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if (statusCode == 401) {
                    changeVideoMode(null);
                }
            }
        });
    }

    public void resetVideoColorConfig(View view) {
        dahuaService.resetVideoColorConfig(new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                seekBarBrightness.setProgress(50);
                seekBarContrast.setProgress(50);
                seekBarHue.setProgress(50);
                seekBarGamma.setProgress(50);
                seekBarSaturation.setProgress(50);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if (statusCode == 401) {
                    resetVideoColorConfig(null);
                }
            }
        });
    }

    public void setLayoutOnCreate() {
        dahuaService.getVideoInOptions(new DahuaService.DahuaResponseHandler() {
            @Override
            public void getResult(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200) {
                    String[] results = new String(responseBody).split("\n");
                    for (String config: results) {
                        Log.d("QWEQWER", config);
                        if (config.startsWith("table.VideoInOptions[0].DayNightColor")) {
                            String attr = config.split("\\.")[2];
                            String[] attrSplit = attr.split("=");
                            String name = attrSplit[0];
                            String value = attrSplit[1];
                            videoMode = Integer.parseInt(value.replace("\r", ""));
                            changeVideoMode(null);
                        }
                    }
                }

                if (statusCode == 401) {
                    setLayoutOnCreate();
                }
            }
        });
    }

    private int getResources(String name, String type) {
        return vlcActivity.getResources().getIdentifier(name, type, vlcActivity.getApplication().getPackageName());
    }
}
