package cordova.plugin.vlc.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cordova.plugin.vlc.DahuaService;
import cordova.plugin.vlc.VLCActivity;
import cordova.plugin.vlc.VideoAdapter;
import cordova.plugin.vlc.VideoFinder;
import cordova.plugin.vlc.ExpandableLayout;

public class VideoRecordsFragment extends Fragment {

    public interface VideoRecordsFragmentActionListener {
        void action(JSONObject ob) throws JSONException;
    }

    private VLCActivity vlcActivity;
    private DahuaService dahuaService;
    private VideoRecordsFragmentActionListener videoRecordsFragmentActionListener;
    private String ip;
    private String user;
    private String password;
    private boolean playbackRunning;
    private boolean endReached;

    private ImageButton buttonSelectDate;
    private ImageButton buttonRefreshVideos;
    private ImageButton buttonStartStopPlayback;

    private TextView textCurrentDate;
    private TextView textTimeStart;
    private TextView textTimeEnd;

    private SeekBar seekBarVLC;

    private RecyclerView layoutVideos;
    private VideoAdapter adapterVideos;
    private RecyclerView.LayoutManager layoutManagerVideos;

    private ProgressBar progressBarLoading;

    private ExpandableLayout layoutVideoContainer;

    private DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            currentDate = new int[] {year, month, dayOfMonth};
            textCurrentDate.setText(getDateAsString(currentDate));
            getVideos();
        }
    };

    private int[] currentDate;

    public VideoRecordsFragment(VLCActivity vlcActivity, Bundle bundle, int layout) {
        super(layout);
        this.vlcActivity = vlcActivity;
        this.ip = bundle.getString("ip");
        this.user = bundle.getString("user");
        this.password = bundle.getString("password");
        this.dahuaService = new DahuaService(user, password, ip);
    }

    public VideoRecordsFragment() {
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof VideoRecordsFragment.VideoRecordsFragmentActionListener) {
            videoRecordsFragmentActionListener = (VideoRecordsFragment.VideoRecordsFragmentActionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement listener!");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        videoRecordsFragmentActionListener = null;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        currentDate = today();
        textCurrentDate = (TextView) view.findViewById(getResources("textCurrentDate", "id"));
        textCurrentDate.setText(getDateAsString(currentDate));
        textTimeStart = (TextView) view.findViewById(getResources("textTimeStart", "id"));
        textTimeEnd = (TextView) view.findViewById(getResources("textTimeEnd", "id"));
        buttonRefreshVideos = (ImageButton) view.findViewById(getResources("buttonRefreshVideos", "id"));
        buttonRefreshVideos.setOnClickListener(this::refreshVideos);
        buttonSelectDate = (ImageButton) view.findViewById(getResources("buttonSelectDate", "id"));
        buttonSelectDate.setOnClickListener(this::selectDate);
        buttonStartStopPlayback = (ImageButton) view.findViewById(getResources("buttonStartStopPlayback", "id"));
        buttonStartStopPlayback.setOnClickListener(this::startStopPlayback);
        playbackRunning = false;
        endReached = false;

        progressBarLoading = (ProgressBar) view.findViewById(getResources("progressBarLoading", "id"));
        seekBarVLC = (SeekBar) view.findViewById(getResources("seekBarVLC", "id"));
        seekBarVLC.setOnSeekBarChangeListener(seekBarVLCListener);

        layoutVideoContainer = (ExpandableLayout) view.findViewById(getResources("layoutVideoContainer", "id"));
        layoutVideoContainer.close();

        layoutManagerVideos = new LinearLayoutManager(getContext());
        layoutVideos = (RecyclerView) view.findViewById(getResources("layoutVideos", "id"));
        layoutVideos.setHasFixedSize(true);
        layoutVideos.setLayoutManager(layoutManagerVideos);
        adapterVideos = new VideoAdapter(new ArrayList<>());
        adapterVideos.setOnItemClickListener(new VideoAdapter.OnItemClickListener() {
            @Override
            public void click(int position) {
                try {
                    JSONObject selectedVideo = adapterVideos.getVideos().get(position);
                    textTimeStart.setText(selectedVideo.getString("StartTime").split(" ")[1]);
                    textTimeEnd.setText(selectedVideo.getString("EndTime").split(" ")[1]);
                    videoRecordsFragmentActionListener.action(
                            new JSONObject()
                                    .put("tag", "playback")
                                    .put("action", "play")
                                    .put("video", selectedVideo));
                    playbackRunning = true;
                    endReached = false;
                    buttonStartStopPlayback.setImageResource(getResources("ic_baseline_pause_24", "drawable"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                adapterVideos.notifyDataSetChanged();
            }
        });
        layoutVideos.setAdapter(adapterVideos);
        getVideos();

        if (vlcActivity.isLandscape()) {
            view.findViewById(getResources("layoutPlaybackControls", "id")).setBackgroundColor(0x00000000);
            view.findViewById(getResources("layoutPlaybackOptions", "id")).setVisibility(View.GONE);
            view.findViewById(getResources("layoutVideoContainer", "id")).setVisibility(View.GONE);
            buttonStartStopPlayback.setBackgroundResource(getResources("button_transparent", "drawable"));
            textTimeStart.setTextColor(0xffffffff);
            textTimeEnd.setTextColor(0xffffffff);
        }

        if (vlcActivity.getSavedVideoForLandscape() != null) {
            try {
                JSONObject selectedVideo = vlcActivity.getSavedVideoForLandscape();
                textTimeStart.setText(selectedVideo.getString("StartTime").split(" ")[1]);
                textTimeEnd.setText(selectedVideo.getString("EndTime").split(" ")[1]);
                playbackRunning = true;
                endReached = false;
                buttonStartStopPlayback.setImageResource(getResources("ic_baseline_pause_24", "drawable"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setVLCProgress(int progress) {
        seekBarVLC.setProgress(progress);
    }

    public void startStopPlayback(View view) {
        if (adapterVideos.getSelected() != RecyclerView.NO_POSITION || vlcActivity.getSavedVideoForLandscape() != null) {
            if (endReached) {
                try {
                    JSONObject selectedVideo = (vlcActivity.getSavedVideoForLandscape() != null) ? vlcActivity.getSavedVideoForLandscape() : adapterVideos.getVideos().get(adapterVideos.getSelected());
                    textTimeStart.setText(selectedVideo.getString("StartTime").split(" ")[1]);
                    textTimeEnd.setText(selectedVideo.getString("EndTime").split(" ")[1]);
                    videoRecordsFragmentActionListener.action(
                            new JSONObject()
                                    .put("tag", "playback")
                                    .put("action", "play")
                                    .put("video", selectedVideo));
                    playbackRunning = true;
                    endReached = false;
                    buttonStartStopPlayback.setImageResource(getResources("ic_baseline_pause_24", "drawable"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return;
            }

            playbackRunning = !playbackRunning;
            if (playbackRunning) {
                try {
                    buttonStartStopPlayback.setImageResource(getResources("ic_baseline_pause_24", "drawable"));
                    videoRecordsFragmentActionListener.action(
                            new JSONObject()
                                    .put("tag", "playback")
                                    .put("action", "start"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    buttonStartStopPlayback.setImageResource(getResources("ic_baseline_play_arrow_24", "drawable"));
                    videoRecordsFragmentActionListener.action(
                            new JSONObject()
                                    .put("tag", "playback")
                                    .put("action", "stop"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void getVideos() {
        progressBarLoading.setVisibility(View.VISIBLE);
        adapterVideos.setVideos(new ArrayList<>());
        adapterVideos.setSelected(RecyclerView.NO_POSITION);
        adapterVideos.notifyDataSetChanged();
        layoutVideoContainer.setTitle("Videos (0)");
        dahuaService.getVideoFinder().getVideos(getDateAsString(currentDate) + " 00:00:00", getDateAsString(currentDate) + " 23:59:59", new VideoFinder.Callback() {
            @Override
            public void message(String message) {
                List<JSONObject> videos = dahuaService.getVideoFinder().getVideos();
                adapterVideos.setVideos(videos);
                adapterVideos.notifyDataSetChanged();
                progressBarLoading.setVisibility(View.GONE);

                layoutVideoContainer.setTitle("Videos (" + videos.size() + ")");
            }
        }, 5);
    }

    public int[] today() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        return new int[] {year, month, day};
    }

    public String getDateAsString(int[] date) {
        return date[0] + "-" + (date[1] + 1) + "-" + date[2];
    }

    public void selectDate(View view) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                getContext(),
                android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                onDateSetListener,
                currentDate[0], currentDate[1], currentDate[2]);
        datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        datePickerDialog.show();
    }

    public void refreshVideos(View view) {
        getVideos();
    }

    private SeekBar.OnSeekBarChangeListener seekBarVLCListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                float p = (float) progress / (float) 100;
                vlcActivity.getMediaPlayer().setPosition(p);
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    public void resetFragment() {
        playbackRunning = false;
        seekBarVLC.setProgress(0);
        buttonStartStopPlayback.setImageResource(getResources("ic_baseline_play_arrow_24", "drawable"));
    }

    public void setEndReached(boolean endReached) {
        this.endReached = endReached;
    }

    private int getResources(String name, String type) {
        return vlcActivity.getResources().getIdentifier(name, type, vlcActivity.getApplication().getPackageName());
    }
}
