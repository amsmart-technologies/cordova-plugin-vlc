package cordova.plugin.vlc;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.security.MessageDigest;

import cz.msebera.android.httpclient.Header;

public class DahuaService {

    private VideoFinder videoFinder;

    public interface Callback {
        void message(String message);
    }

    public enum DayNightColor {
        Color, Auto, Monochrome
    }

    public static class ColorConfig {
        public static String BRIGHTNESS = "Brightness";
        public static String CHROMA_SUPPRESS = "ChromaSuppress";
        public static String GAMMA = "Gamma";
        public static String CONTRAST = "Contrast";
        public static String HUE = "Hue";
        public static String SATURATION = "Saturation";
    }

    public static class DahuaUrl {
        public static String SNAPSHOT = "/cgi-bin/snapshot.cgi?1";
        public static String SET_VIDEO_COLOR_CONFIG = "/cgi-bin/configManager.cgi?action=setConfig&VideoColor[0][0].";
        public static String GET_VIDEO_COLOR_CONFIG = "/cgi-bin/configManager.cgi?action=getConfig&name=VideoColor";
        public static String RTSP_STREAM = "/cam/realmonitor?channel=1&subtype=0";
        public static String RTSP_STREAM_SUB_STREAM = "/cam/realmonitor?channel=1&subtype=1";
        public static String RTSP_PLAYBACK = "/cam/playback?channel=1&starttime=[DATE_START]&endtime=[DATE_END]";
        public static String SET_DAY_NIGHT_COLOR = "/cgi-bin/configManager.cgi?action=setConfig&VideoInOptions[0].DayNightColor=";
        public static String GET_VIDEO_IN_OPTIONS = "/cgi-bin/configManager.cgi?action=getConfig&name=VideoInOptions";
        public static String VIDEO_START_STOP = "/cgi-bin/configManager.cgi?action=setConfig&RecordMode[0].Mode=";
        public static String GET_RECORD_STATE = "/cgi-bin/configManager.cgi?action=getConfig&name=RecordMode";
        public static String VIDEO_CREATE_FINDER = "/cgi-bin/mediaFileFind.cgi?action=factory.create";
        public static String VIDEO_DESTROY_FINDER = "/cgi-bin/mediaFileFind.cgi?action=destroy&object=[FINDER_ID]";
        public static String VIDEO_START_FINDER = "/cgi-bin/mediaFileFind.cgi?action=findFile&object=[FINDER_ID]&condition.Channel=1&condition.StartTime=[DATE_START]&condition.EndTime=[DATE_END]&condition.Dir[0]=\"/mnt/sd\"&condition.Types[0]=dav&condition.Flag[0]=Manual";
        public static String VIDEO_FIND_NEXT = "/cgi-bin/mediaFileFind.cgi?action=findNextFile&object=[FINDER_ID]&count=[COUNT]";
    }

    private String username;
    private String password;
    private String ip;
    private AsyncHttpClient client;

    public DahuaService(String username, String password, String ip) {
        this.username = username;
        this.password = password;
        this.ip = ip;
        this.client = new AsyncHttpClient();
        this.videoFinder = new VideoFinder(this);
    }

    public void snapshot(AsyncHttpResponseHandler responseHandler) {
        String url = getBaseUrl() + DahuaUrl.SNAPSHOT;
        client.setBasicAuth(username, password);
        client.get(url, responseHandler);
    }

    public void setVideoColorConfig(String config, int value, AsyncHttpResponseHandler responseHandler) {
        String url = getBaseUrl() + DahuaUrl.SET_VIDEO_COLOR_CONFIG + config + "=" + value;
        client.setBasicAuth(username, password);
        client.get(url, responseHandler);
    }

    public void resetVideoColorConfig(AsyncHttpResponseHandler responseHandler) {
        String url = getBaseUrl() + "/cgi-bin/configManager.cgi?action=setConfig" +
                "&VideoColor[0][0].Brightness=50" +
                "&VideoColor[0][0].Contrast=50" +
                "&VideoColor[0][0].Gamma=50" +
                "&VideoColor[0][0].Hue=50" +
                "&VideoColor[0][0].Saturation=50";
        client.setBasicAuth(username, password);
        client.get(url, responseHandler);
    }

    public void getVideoColorConfig(AsyncHttpResponseHandler responseHandler) {
        String url = getBaseUrl() + DahuaUrl.GET_VIDEO_COLOR_CONFIG;
        client.setBasicAuth(username, password);
        client.get(url, responseHandler);
    }

    public void auth(String path, Callback callback) {
        client.get(getBaseUrl() + DahuaUrl.SNAPSHOT, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {}

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if (statusCode == 401) {
                    String digest = null;
                    for (Header header: headers) {
                        if (header.getName().equals("WWW-Authenticate")) {
                            digest = header.getValue();
                            break;
                        }
                    }
                    callback.message(getDigestAuthString(digest, username, password, path));
                }
            }
        });
    }

    public void setDayNightColor(DayNightColor color, AsyncHttpResponseHandler responseHandler) {
        String url = getBaseUrl() + DahuaUrl.SET_DAY_NIGHT_COLOR + color.ordinal();
        client.setBasicAuth(username, password);
        client.get(url, responseHandler);
    }

    public void getVideoInOptions(DahuaResponseHandler responseHandler) {
        String url = getBaseUrl() + DahuaUrl.GET_VIDEO_IN_OPTIONS;
        client.setBasicAuth(username, password);
        client.get(url, responseHandler);
    }

    public void startRecord(DahuaResponseHandler responseHandler) {
        String url = getBaseUrl() + DahuaUrl.VIDEO_START_STOP + "1";
        client.setBasicAuth(username, password);
        client.get(url, responseHandler);
    }

    public void stopRecord(DahuaResponseHandler responseHandler) {
        String url = getBaseUrl() + DahuaUrl.VIDEO_START_STOP + "2";
        client.setBasicAuth(username, password);
        client.get(url, responseHandler);
    }

    public void getRecordState(AsyncHttpResponseHandler responseHandler) {
        String url = getBaseUrl() + DahuaUrl.GET_RECORD_STATE;
        client.setBasicAuth(username, password);
        client.get(url, responseHandler);
    }

    private String md5(final String s) {
        final String MD5 = "MD5";
        try {
            MessageDigest digest = java.security.MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private String getDigestAuthString(String digest, String user, String pass, String path) {
        String[] digestFields = digest.replace("Digest ", "").split(", ");

        String realm = digestFields[0].replace("realm=\"", "").replace("\"", "");
        String qop = digestFields[1].replace("qop=\"", "").replace("\"", "");
        String nonce = digestFields[2].replace("nonce=\"", "").replace("\"", "");
        String method = "GET";
        String cnonce = md5(System.currentTimeMillis() + "");

        String HA1 = md5(user + ":" + realm + ":" + pass);
        String HA2 = md5(method + ":" + path);
        String response = md5(HA1 + ":" + nonce + ":00000001:" + cnonce + ":" + qop + ":" + HA2);

        return "Digest username=\"" + user + "\",realm=\"" + realm + "\",nonce=\"" + nonce + "\",uri=\"" + path + "\",cnonce=\"" + cnonce + "\",nc=00000001,algorithm=MD5,response=\"" + response + "\",qop=\"" + qop + "\"";
    }

    public String getBaseUrl (){
        return "http://" + this.ip;
    }

    public static abstract class DahuaResponseHandler extends AsyncHttpResponseHandler {

        public DahuaResponseHandler() { }

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            getResult(statusCode, headers, responseBody);
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            getResult(statusCode, headers, responseBody);
        }

        public abstract void getResult(int statusCode, Header[] headers, byte[] responseBody);
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getIp() {
        return ip;
    }

    public VideoFinder getVideoFinder() {
        return videoFinder;
    }
}
