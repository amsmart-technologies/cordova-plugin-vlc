import UIKit
import Pulley
import Photos

@objc(VLCPlugin) class VLCPlugin : CDVPlugin {
    var mediaPlayer: VLCMediaPlayer!
    var callback: String?
    
    @objc(loadUrl:)
    func loadUrl(command: CDVInvokedUrlCommand){
        let address = (command.argument(at: 0) as! String)
        let username = (command.argument(at: 1) as! String)
        let password = (command.argument(at: 2) as! String)
        let cameraName = (command.argument(at: 3) as! String)
        let dahuaService = DahuaService(address: address, username: username, password: password, cameraName: cameraName)
        self.mediaPlayer = VLCMediaPlayer.shared
        self.mediaPlayer.delegate = self
        self.callback = command.callbackId
        
        let mainContentVC = UIStoryboard(name: "VLCPlugin", bundle: nil).instantiateViewController(withIdentifier: "main") as! MainViewController
        mainContentVC.mediaPlayer = self.mediaPlayer
        mainContentVC.dahuaService = dahuaService
        
        let drawerContentVC = UIStoryboard(name: "VLCPlugin", bundle: nil).instantiateViewController(withIdentifier: "drawer") as! DrawerViewController
        drawerContentVC.dahuaService = dahuaService
        drawerContentVC.mediaPlayer = self.mediaPlayer
        
        let pulleyController = PulleyViewController(contentViewController: mainContentVC, drawerViewController: drawerContentVC)
        pulleyController.modalPresentationStyle = .fullScreen
        pulleyController.shadowOpacity = 0.0
        pulleyController.initialDrawerPosition = .partiallyRevealed
        
        self.viewController.present(pulleyController, animated: true, completion: nil)
    }
}

class DahuaService: NSObject {
    
    struct ColorConfig {
        static var BRIGHTNESS = "Brightness"
        static var CHROMA_SUPPRESS = "ChromaSuppress"
        static var GAMMA = "Gamma"
        static var CONTRAST = "Contrast"
        static var HUE = "Hue"
        static var SATURATION = "Saturation"
    }
    
    struct DahuaUrl {
        static var SNAPSHOT = "/cgi-bin/snapshot.cgi?1"
        static var SET_VIDEO_COLOR_CONFIG = "/cgi-bin/configManager.cgi?action=setConfig&VideoColor[0][0]."
        static var GET_VIDEO_COLOR_CONFIG = "/cgi-bin/configManager.cgi?action=getConfig&name=VideoColor"
        static var RESET_VIDEO_COLOR_CONFIG = "/cgi-bin/configManager.cgi?action=setConfig&VideoColor[0][0].Brightness=50&VideoColor[0][0].Contrast=50&VideoColor[0][0].Gamma=50&VideoColor[0][0].Hue=50&VideoColor[0][0].Saturation=50"
        static var RTSP_STREAM = "/cam/realmonitor?channel=1&subtype=0"
        static var RTSP_STREAM_SUB_STREAM = "/cam/realmonitor?channel=1&subtype=1"
        static var SET_DAY_NIGHT_COLOR = "/cgi-bin/configManager.cgi?action=setConfig&VideoInOptions[0].DayNightColor="
        static var GET_VIDEO_IN_OPTIONS = "/cgi-bin/configManager.cgi?action=getConfig&name=VideoInOptions"
    }
    
    var username: String?
    var password: String?
    var address: String?
    var cameraName: String?
    
    init(address: String, username: String, password:String, cameraName: String){
        super.init()
        if(address.isEmpty || username.isEmpty || password.isEmpty){
            fatalError("Empty parameters")
        }
        self.address = address
        self.username = username
        self.password = password
        self.cameraName = cameraName
    }
    
    func snapshot(completion: @escaping (UIImage) -> ()){
        self.sendHttpGet(toUrl: DahuaUrl.SNAPSHOT) { (result) in
            completion(result as! UIImage)
        }
    }
    
    func setDayNightColor(color: String, completion: @escaping (Any) -> ()){
        self.sendHttpGet(toUrl: DahuaUrl.SET_DAY_NIGHT_COLOR + color) { (result) in
            completion(result)
        }
    }
    
    func getVideoInOptions(completion: @escaping (String) -> ()){
        self.sendHttpGet(toUrl: DahuaUrl.GET_VIDEO_IN_OPTIONS) { (result) in
            completion(result as! String)
        }
    }
    
    func getVideoColorConfig(completion: @escaping (String) -> ()){
        self.sendHttpGet(toUrl: DahuaUrl.GET_VIDEO_COLOR_CONFIG) { (result) in
            completion(result as! String)
        }
    }
    
    func setVideoColorConfig(config: String, value: Int, completion: @escaping (String) -> ()){
        self.sendHttpGet(toUrl: DahuaUrl.SET_VIDEO_COLOR_CONFIG + config + "=" + String(value)) { (result) in
            completion(result as! String)
        }
    }
    
    func resetVideoColorConfig(completion: @escaping (String) -> ()){
        self.sendHttpGet(toUrl: DahuaUrl.RESET_VIDEO_COLOR_CONFIG) { (result) in
            completion(result as! String)
        }
    }
    
    func sendHttpGet(toUrl: String, completion: @escaping (Any) -> ()){
        let url = URL(string: self.getBaseURL() + toUrl)
        guard let requestUrl = url else { fatalError("No url") }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "GET"
        self.doRequest(request: request) { (result) in
            completion(result)
        }
    }
    
    func doRequest(request: URLRequest, completion: @escaping (Any) -> ()) {
        print("Doing request with url: \(String(describing: request.url!)) ... ")
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print("URLSession Error \(error)")
                return
            }
            if let response = response as? HTTPURLResponse {
                print("Response Status code: \(response.statusCode)")
            }
            if let data = data, let dataString = String(data: data, encoding: .utf8) {
                print("Response data: \(dataString) \n")
                completion(dataString)
            }
            if let imageData = data as Data? {
                if let image = UIImage(data: imageData) {
                    completion(image)
                }
            }
        }
        task.resume()
    }
    
    func getBaseURL() -> String{
        return "http://\(self.username ?? ""):\(self.password ?? "")@\(self.address ?? "")"
    }
    
    func getRtspUrl(main: Bool) -> String{
        return "rtsp://\(self.username ?? ""):\(self.password ?? "")@\(self.address ?? "")" + (main ? DahuaUrl.RTSP_STREAM : DahuaUrl.RTSP_STREAM_SUB_STREAM);
    }
    
}

class DrawerViewController: UIViewController {
    var mediaPlayer: VLCMediaPlayer!
    var dahuaService: DahuaService!
    var mainStream = true
    var dayNightColor = 0
    
    @IBOutlet weak var snapshotButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var streamButton: UIButton!
    @IBOutlet weak var autoButton: UIButton!
    @IBOutlet weak var gammaSlider: UISlider!
    @IBOutlet weak var saturationSlider: UISlider!
    @IBOutlet weak var brightnessSlider: UISlider!
    @IBOutlet weak var hueSlider: UISlider!
    @IBOutlet weak var contrastSlider: UISlider!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupButtons()
        self.getVideoInOptions()
    }
    
    func setupButtons(){
        self.setButtonImage(button: self.snapshotButton, image: "camera.fill")
        self.setButtonImage(button: self.playButton, image: "pause.fill")
        self.setButtonImage(button: self.recordButton, image: "recordingtape")
        self.setButtonImage(button: self.streamButton, image: "rectangle.fill")
        self.setButtonImage(button: self.autoButton, image: "a.circle.fill")
        DispatchQueue.main.async {
            self.autoButton.isEnabled = false
            self.gammaSlider.isEnabled = false
            self.saturationSlider.isEnabled = false
            self.brightnessSlider.isEnabled = false
            self.hueSlider.isEnabled = false
            self.contrastSlider.isEnabled = false
        }
    }
    
    func getVideoInOptions(){
        self.dahuaService.getVideoInOptions { (result) in
            let results = result.split(separator: "\r\n")
            results.forEach { (config) in
                if(config.starts(with: "table.VideoInOptions[0].DayNightColor")){
                    let attr = config.split(separator: ".")[2]
                    let attrSplit = attr.split(separator: "=")
                    let value = attrSplit[1];
                    self.dayNightColor = Int(value) ?? 0
                }
            }
            self.setAutoButtonImage()
            self.getVideoColorConfig()
        }
    }
    
    func getVideoColorConfig(){
        self.dahuaService.getVideoColorConfig { (result) in
            let results = result.split(separator: "\r\n")
            results.forEach { (config) in
                if(config.starts(with: "table.VideoColor[0][0]")){
                    let attr = config.split(separator: ".")[2]
                    let attrSplit = attr.split(separator: "=")
                    let name = attrSplit[0];
                    let value = Float(attrSplit[1]) ?? 50
                    switch name {
                    case DahuaService.ColorConfig.BRIGHTNESS:
                        self.setSliderValue(value: value, slider: self.brightnessSlider)
                        break
                    case DahuaService.ColorConfig.CONTRAST:
                        self.setSliderValue(value: value, slider: self.contrastSlider)
                        break
                    case DahuaService.ColorConfig.GAMMA:
                        self.setSliderValue(value: value, slider: self.gammaSlider)
                        break
                    case DahuaService.ColorConfig.HUE:
                        self.setSliderValue(value: value, slider: self.hueSlider)
                        break
                    case DahuaService.ColorConfig.SATURATION:
                        self.setSliderValue(value: value, slider: self.saturationSlider)
                        break
                    default:
                        break
                    }
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    @IBAction func snapShotAction(_ sender: Any) {
        self.snapshotButton.isEnabled = false
        self.dahuaService.snapshot { (image) in
            let photos = PHPhotoLibrary.authorizationStatus()
            if photos == .authorized {
                self.writeImage(image: image)
            } else if photos == .notDetermined {
                PHPhotoLibrary.requestAuthorization({status in
                    if status == .authorized{
                        self.writeImage(image: image)
                    }
                })
            }
        }
    }
    
    func writeImage(image: UIImage){
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(saveError), nil)
    }
    
    @objc func saveError(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        self.alert(title: "Snapshot created!", message: "Image saved to Camera Roll")
        DispatchQueue.main.async {
            self.snapshotButton.isEnabled = true
        }
    }
    
    func alert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func playAction(_ sender: Any) {
        if(self.mediaPlayer.isPlaying){
            self.mediaPlayer.pause()
            self.setButtonImage(button: self.playButton, image: "play.fill")
        }else{
            self.mediaPlayer.play()
            self.setButtonImage(button: self.playButton, image: "pause.fill")
        }
    }
    
    @IBAction func recordAction(_ sender: Any) {
        
    }
    
    @IBAction func streamAction(_ sender: Any) {
        self.mainStream = !self.mainStream
        let mediaURL = self.dahuaService.getRtspUrl(main: self.mainStream)
        mediaPlayer.media = VLCMedia(url: URL(string: mediaURL)!)
        mediaPlayer.play()
        if(self.mainStream){
            self.setButtonImage(button: self.streamButton, image: "rectangle.fill")
        }else{
            self.setButtonImage(button: self.streamButton, image: "square.fill")
        }
    }
    
    @IBAction func autoAction(_ sender: Any) {
        self.dayNightColor += 1
        if (self.dayNightColor > 2) {
            self.dayNightColor = 0
        }
        self.dahuaService.setDayNightColor(color: String(self.dayNightColor)) { (result) in
            self.setAutoButtonImage()
        }
    }
    
    func setAutoButtonImage(){
        if(self.dayNightColor == 0){
            self.setButtonImage(button: self.autoButton, image: "sun.max.fill")
        }
        if(self.dayNightColor == 1){
            self.setButtonImage(button: self.autoButton, image: "a.circle.fill")
        }
        if(self.dayNightColor == 2){
            self.setButtonImage(button: self.autoButton, image: "moon.stars.fill")
        }
        DispatchQueue.main.async {
            self.autoButton.isEnabled = true
        }
    }
    
    func setButtonImage(button: UIButton, image: String){
        DispatchQueue.main.async {
            if #available(iOS 13.0, *) {
                button.setImage(UIImage(systemName: image), for: .normal)
            } else {
                button.setImage(nil, for: .normal)
                button.setTitle(image[0].uppercased(), for: .normal)
            }
        }
    }
    
    @IBAction func resetConfig(_ sender: Any) {
        self.dahuaService.resetVideoColorConfig { (result) in
            self.setSliderValue(value: Float(50), slider: self.gammaSlider)
            self.setSliderValue(value: Float(50), slider: self.saturationSlider)
            self.setSliderValue(value: Float(50), slider: self.brightnessSlider)
            self.setSliderValue(value: Float(50), slider: self.hueSlider)
            self.setSliderValue(value: Float(50), slider: self.contrastSlider)
        }
    }
    
    @IBAction func gammaSliderChanged(_ sender: UISlider) {
        let roundedCurrent = (sender.value/Float(10)).rounded()
        let newValue = Int(roundedCurrent) * 10
        DispatchQueue.main.async { sender.isEnabled = false }
        self.dahuaService.setVideoColorConfig(config: DahuaService.ColorConfig.GAMMA, value: newValue) { (result) in
            self.setSliderValue(value: Float(newValue), slider: sender)
        }
    }
    
    @IBAction func saturationSliderChanged(_ sender: UISlider) {
        let roundedCurrent = (sender.value/Float(10)).rounded()
        let newValue = Int(roundedCurrent) * 10
        DispatchQueue.main.async { sender.isEnabled = false }
        self.dahuaService.setVideoColorConfig(config: DahuaService.ColorConfig.SATURATION, value: newValue) { (result) in
            self.setSliderValue(value: Float(newValue), slider: sender)
        }
    }
    
    @IBAction func brightnessSliderChanged(_ sender: UISlider) {
        let roundedCurrent = (sender.value/Float(10)).rounded()
        let newValue = Int(roundedCurrent) * 10
        DispatchQueue.main.async { sender.isEnabled = false }
        self.dahuaService.setVideoColorConfig(config: DahuaService.ColorConfig.BRIGHTNESS, value: newValue) { (result) in
            self.setSliderValue(value: Float(newValue), slider: sender)
        }
    }
    
    @IBAction func hueSliderChanged(_ sender: UISlider) {
        let roundedCurrent = (sender.value/Float(10)).rounded()
        let newValue = Int(roundedCurrent) * 10
        DispatchQueue.main.async { sender.isEnabled = false }
        self.dahuaService.setVideoColorConfig(config: DahuaService.ColorConfig.HUE, value: newValue) { (result) in
            self.setSliderValue(value: Float(newValue), slider: sender)
        }
    }
    
    @IBAction func contrastSliderChanged(_ sender: UISlider) {
        let roundedCurrent = (sender.value/Float(10)).rounded()
        let newValue = Int(roundedCurrent) * 10
        DispatchQueue.main.async { sender.isEnabled = false }
        self.dahuaService.setVideoColorConfig(config: DahuaService.ColorConfig.CONTRAST, value: newValue) { (result) in
            self.setSliderValue(value: Float(newValue), slider: sender)
        }
    }
    
    func setSliderValue(value: Float, slider: UISlider){
        DispatchQueue.main.async {
            slider.setValue(value, animated: true)
            slider.isEnabled = true
        }
    }
    
}

class MainViewController: UIViewController {
    var mediaPlayer: VLCMediaPlayer!
    var dahuaService: DahuaService!
    
    @IBOutlet weak var cameraLabel: UILabel!
    @IBOutlet weak var movieView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setButtonImage(button: self.closeButton, image: "xmark")
        self.cameraLabel.text = self.dahuaService.cameraName
        let mediaURL = self.dahuaService.getRtspUrl(main: true)
        mediaPlayer.drawable = movieView
        mediaPlayer.media = VLCMedia(url: URL(string: mediaURL)!)
        mediaPlayer.play()
    }
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    @IBAction func close(_ sender: Any) {
        self.mediaPlayer.stop()
    }
    
    func setButtonImage(button: UIButton, image: String){
        DispatchQueue.main.async {
            if #available(iOS 13.0, *) {
                button.setImage(UIImage(systemName: image), for: .normal)
            } else {
                button.setImage(nil, for: .normal)
                button.setTitle(image[0].uppercased(), for: .normal)
            }
        }
    }
}

extension VLCPlugin: VLCMediaPlayerDelegate {
    func mediaPlayerStateChanged(_ aNotification: Notification!) {
        if mediaPlayer.state == .stopped {
            let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
            self.commandDelegate?.send(pluginResult, callbackId: self.callback)
            self.viewController.dismiss(animated: true, completion: nil)
        }
    }
}

extension String {
    subscript(_ i: Int) -> String {
        let idx1 = index(startIndex, offsetBy: i)
        let idx2 = index(idx1, offsetBy: 1)
        return String(self[idx1..<idx2])
    }
}

extension VLCMediaPlayer {
    public static let shared: VLCMediaPlayer = {
        let options: [String] = ["--no-drop-late-frames", "--no-skip-frames", "--rtsp-tcp"]
        let player: VLCMediaPlayer = VLCMediaPlayer(options: options)
        return player
    }()
}
